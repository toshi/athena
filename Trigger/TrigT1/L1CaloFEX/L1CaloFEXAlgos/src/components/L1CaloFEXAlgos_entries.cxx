/*
    Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

#include "../jFexTower2SCellDecorator.h"
#include "../jFexEmulatedTowers.h"

using namespace LVL1;

DECLARE_COMPONENT( jFexTower2SCellDecorator )
DECLARE_COMPONENT( jFexEmulatedTowers )
