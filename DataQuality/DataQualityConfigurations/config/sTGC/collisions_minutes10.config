###########################################################
# sTGC
###########################################################

#############
# Output
#############

output top_level {
  output MuonDetectors {
    output STGC {
      output Shifter {
        output Global {
        }
        output Occupancy {
         output Strip {
         }
         output Pad {
         }
         output Wire {
         }
        }
        output Lumiblock {
        }
        output Timing {
         output Strip {
         }
         output Pad {
         }
         output Wire {
         }
        }
      }
      output Expert {
        output Summary {
         output ClusterSize {
        }
         output Time {
         }
    output Charge {
      output Strip {
      }
      output Pad {
      }
      output Wire {
      }
    }
    output Residuals {
    }
    
        }
        output StripsPerSectorEA {
        }
      }
    }
  }
}

#######################
# Histogram Assessments
#######################
dir Muon {

  dir MuonRawDataMonitoring {
    #reference = stream=physics_Main:CentrallyManagedReferences_Main;CentrallyManagedReferences

    dir sTgc {
      algorithm = stgc_Histogram_Not_Empty
      dir sTgcLayers {
        dir xyLayeredGlobalPosition { 
          hist xyPos_ASide_* { 
           output = MuonDetectors/STGC/Shifter/Global
           algorithm = stgc_GatherData
          }
         
          hist xyPos_CSide_* {
           output = MuonDetectors/STGC/Shifter/Global
           algorithm = stgc_GatherData
          }
         }
       }

      dir sTgcOccupancy {
         regex = 1 
         dir stripOccupancy {
           hist stripOccupancy_multiplet_*[12]_gasgap_*[1234] {
            output = MuonDetectors/STGC/Shifter/Occupancy/Strip
            algorithm = stgc_S_BinsGreaterThanThreshold
            }    
          }
          
         dir padOccupancy {
           hist padOccupancy_multiplet_1_gasgap_1 {
            output = MuonDetectors/STGC/Shifter/Occupancy/Pad
            algorithm = stgc_Pm1s12_BinsGreaterThanThreshold 
           }
           hist padOccupancy_multiplet_1_gasgap_2 {
            output = MuonDetectors/STGC/Shifter/Occupancy/Pad
            algorithm = stgc_Pm1s12_BinsGreaterThanThreshold 
           }

           hist padOccupancy_multiplet_1_gasgap_3 {
            output = MuonDetectors/STGC/Shifter/Occupancy/Pad
            algorithm = stgc_Pm1s34_BinsGreaterThanThreshold
           }
           hist padOccupancy_multiplet_1_gasgap_4 {
            output = MuonDetectors/STGC/Shifter/Occupancy/Pad
            algorithm = stgc_Pm1s34_BinsGreaterThanThreshold
           }
 
           hist padOccupancy_multiplet_2_gasgap_1 {
            output = MuonDetectors/STGC/Shifter/Occupancy/Pad
            algorithm = stgc_Pm2s12_BinsGreaterThanThreshold
           }

           hist padOccupancy_multiplet_2_gasgap_2 {
            output = MuonDetectors/STGC/Shifter/Occupancy/Pad
            algorithm = stgc_Pm2s12_BinsGreaterThanThreshold
           }

           hist padOccupancy_multiplet_2_gasgap_3 {
            output = MuonDetectors/STGC/Shifter/Occupancy/Pad
            algorithm = stgc_Pm2s34_BinsGreaterThanThreshold
           }
             
           hist padOccupancy_multiplet_2_gasgap_4 {
            output = MuonDetectors/STGC/Shifter/Occupancy/Pad
            algorithm = stgc_Pm2s34_BinsGreaterThanThreshold
           }  
         }
      
         dir wireGroupOccupancy {
           hist wireGroupOccupancyPerQuad_multiplet_*[12]_gasgap_*[1234] {
            output = MuonDetectors/STGC/Shifter/Occupancy/Wire
            algorithm = stgc_W_BinsGreaterThanThreshold
           }
         }
       }
    }
  } 
 }
 
#############
# Algorithms
#############


algorithm stgc_Histogram_Not_Empty {
  libname = libdqm_algorithms.so
  name = Histogram_Not_Empty
  reference = stream=physics_Main:CentrallyManagedReferences_Main;CentrallyManagedReferences
}

algorithm stgc_GatherData {
  name = GatherData
  libname = libdqm_algorithms.so
}

algorithm stgc_S_BinsGreaterThanThreshold {
  libname = libdqm_algorithms.so
  name = Bins_GreaterThanNonZeroMedian_Threshold 
  BinThreshold = 0.01
  TotalBins = 35280
  thresholds = 	stgc_SEta12_thresholds
  reference = stream=physics_Main:CentrallyManagedReferences_Main;CentrallyManagedReferences
}

algorithm stgc_Pm1s12_BinsGreaterThanThreshold {
  libname = libdqm_algorithms.so
  name = Bins_GreaterThanNonZeroMedian_Threshold 
  BinThreshold = 0.01
  TotalBins = 5968
  thresholds =  stgc_Pm1s12_thresholds
  reference = stream=physics_Main:CentrallyManagedReferences_Main;CentrallyManagedReferences
}

algorithm stgc_Pm1s34_BinsGreaterThanThreshold {
  libname = libdqm_algorithms.so
  name = Bins_GreaterThanNonZeroMedian_Threshold 
  BinThreshold = 0.01
  TotalBins = 6656
  thresholds =  stgc_Pm1s34_thresholds
  reference = stream=physics_Main:CentrallyManagedReferences_Main;CentrallyManagedReferences
}

algorithm stgc_Pm2s12_BinsGreaterThanThreshold {
  libname = libdqm_algorithms.so
  name = Bins_GreaterThanNonZeroMedian_Threshold 
  BinThreshold = 0.01
  TotalBins = 5216
  thresholds =  stgc_Pm2s12_thresholds
  reference = stream=physics_Main:CentrallyManagedReferences_Main;CentrallyManagedReferences
}

algorithm stgc_Pm2s34_BinsGreaterThanThreshold {
  libname = libdqm_algorithms.so
  name = Bins_GreaterThanNonZeroMedian_Threshold 
  BinThreshold = 0.01
  TotalBins = 5488
  thresholds =  stgc_Pm2s34_thresholds
  reference = stream=physics_Main:CentrallyManagedReferences_Main;CentrallyManagedReferences
}

algorithm stgc_W_BinsGreaterThanThreshold {
  libname = libdqm_algorithms.so
  name = Bins_GreaterThanNonZeroMedian_Threshold  
  BinThreshold = 0.01
  TotalBins = 3588
  thresholds =  stgc_WEta12_thresholds
  reference = stream=physics_Main:CentrallyManagedReferences_Main;CentrallyManagedReferences
}


#############
#thresholds
#############

thresholds stgc_SEta12_thresholds {
  limits NBins {
    warning = 32458
    error = 28224
  }
}

thresholds stgc_Pm1s12_thresholds {
  limits NBins {
    warning =  5490
    error = 4774
  }
}

thresholds stgc_Pm1s34_thresholds {
  limits NBins {
    warning =  6123
    error = 5325
  }
}

thresholds stgc_Pm2s12_thresholds {
  limits NBins {
    warning =  4799
    error = 4173
  }
}

thresholds stgc_Pm2s34_thresholds {
  limits NBins {
    warning =  5049
    error = 4390
  }
}


thresholds stgc_WEta12_thresholds {
  limits NBins {
    warning =  3300
    error = 2870
  }
}



  
